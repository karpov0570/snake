#include <iostream>
#include<ctime>
#include<windows.h>
#include<string>
#include<iomanip>
#include <conio.h>

using namespace std;

int VaR = 2;

class Apple
{
public:
	Apple(int size, char vision)
	{
		this->vision = vision;
		this->size = size;
	}
	void setXY(int x, int y)
	{
		this->x = x;
		this->y = y;
	}
	char vision;
	int size;
	int x = 0, y = 0;
};

class Snake
{
public:
	//_____________________________________________________________________
	class Body
	{
	public:
		Body()
		{
			x = y = 1;
		}
		void setXY(int x, int y)
		{
			this->x = x;
			this->y = y;
		}
		int X()
		{
			return x;
		}
		int Y()
		{
			return y;
		}
		char visionPart()
		{
			return vision;
		}
		void setVision(char vision)
		{
			this->vision = vision;
		}
		Body& operator=(const Body& other)
		{
			this->x = other.x;
			this->y = other.y;
			return *this;
		}

	private:
		int x, y;
		char vision = char (183);
	};
	//____________________________________________________________________
	int x, y;
	Snake()
	{
		x = 5;
		y = 5;		//dont setup <5
		for (int i = 0; i < size; i++)
		{
			parts[i].setXY(x, y - i);
		}
		parts[0].setVision('o');
	}
	void EatAnApple(Apple& apple)
	{
		this->score += apple.size;
		addBodyPart();
	}
	int getScore()
	{
		return score;
	}
	int getSize()
	{
		return size;;
	}
	int getXpart(int indexPart)
	{
		return parts[indexPart].X();
	}
	int getYpart(int indexPart)
	{
		return parts[indexPart].Y();
	}
	void addBodyPart()
	{
		Body* newParts = new Body[size + 1];
		for (int i = 0; i < size; i++)
		{
			newParts[i] = parts[i];
		}
		newParts[size] = parts[size - 1];

		size++;

		delete[] parts;

		parts = newParts;
	}
	void move(char button)
	{
		switch (button)
		{
		case (97):		//button A, left
		case (75):		//arrow left
		{
			if (!(parts[1].X() == x && parts[1].Y() == y - 1))	//this is a condition for the snake not to move in its parts
			{
				y--;
				move_parts();
			}
			break;
		}
		case (119):		//button W, up
		case (72):		//arrow up
		{
			if (!(parts[1].X() == x - 1 && parts[1].Y() == y))	//this is a condition for the snake not to move in its parts
			{
				x--;
				move_parts();
			}
			break;
		}
		case (100):		//button D, right
		case (77):		//arrow right
		{
			if (!(parts[1].X() == x && parts[1].Y() == y + 1))	//this is a condition for the snake not to move in its parts
			{
				y++;
				move_parts();
			}
			break;
		}
		case (115):		//button S, down
		case (80):		//arrow down
		{
			if (!(parts[1].X() == x + 1 && parts[1].Y() == y))	//this is a condition for the snake not to move in its parts
			{
				x++;
				move_parts();
			}
			break;
		}
		}
	}
	char visionPart(int indexPart)
	{
		return parts[indexPart].visionPart();
	}
	~Snake()
	{
		delete[] parts;
	}
private:
	int score = 0;
	int size = 4;
	Body* parts = new Body[size];
	void move_parts()
	{
		for (int i = 1; i < size; i++)
		{
			parts[size - i] = parts[(size - i) - 1];
		}
		parts[0].setXY(x, y);
		parts[0].setVision('o');
		parts[1].setVision(char (183));
		parts[size - 1].setVision(',');
	}
};

void SetPos(int x, int y)
{
	COORD c;
	c.X = y;
	c.Y = x;
	SetConsoleCursorPosition(GetStdHandle(STD_OUTPUT_HANDLE), c);
}

void fillArray(string** const arr, const int rows, const int cols)
{
	for (int i = 1; i < rows - 1; i++)
	{
		for (int j = 1; j < cols - 1; j++)
		{

			arr[i][j] = " ";
		}
	}

	arr[0][0] = " ";
	for (int i = 1; i < cols - 1; i++)
	{
		arr[0][i] = "_";
		arr[rows - 1][i] = "_";
	}

	for (int i = 1; i < rows; i++)
	{
		arr[i][0] = "|";
		arr[i][cols - 1] = "|";
	}

}

void showArray(string** const arr, const int rows, const int cols)
{
	for (int i = 0; i < rows; i++)
	{
		SetPos(0 + i, 0);
		for (int j = 0; j < cols; j++)
		{
			SetPos(0 + i, j * VaR);
			cout << arr[i][j];
		}
		cout << endl;
	}
}

void deleteTail_FromArray(string** arr, Snake& snake)
{
	arr[snake.getXpart(snake.getSize() - 1)][snake.getYpart(snake.getSize() - 1)] = ' ';
	SetPos(snake.getXpart(snake.getSize() - 1), snake.getYpart(snake.getSize() - 1) * VaR);
	cout << " ";
}

void setSnakeOnArray(string** arr, Snake& snake)
{
	for (int i = 0; i < snake.getSize(); i++)
	{
		arr[snake.getXpart(i)][snake.getYpart(i)] = snake.visionPart(i);
		SetPos(snake.getXpart(i), snake.getYpart(i) * VaR);
		cout << arr[snake.getXpart(i)][snake.getYpart(i)];
	}
}

void setAppleOnArray(string** arr, const int rows, const int cols, Apple& sApple, Apple& bApple)
{
	if (sApple.x != 0 && sApple.y != 0)
	{
		arr[sApple.x][sApple.y] = sApple.vision;
		SetPos(sApple.x, sApple.y * VaR);
		cout << arr[sApple.x][sApple.y];
	}
	if (bApple.x != 0 && bApple.y != 0)
	{
		arr[bApple.x][bApple.y] = bApple.vision;
		SetPos(bApple.x, bApple.y * VaR);
		cout << arr[bApple.x][bApple.y];
	}
}

bool gameover(const int rows, const int cols, Snake& snake)
{
	if (snake.x == 0 || snake.y == 0 ||
		snake.x == rows - 1 || snake.y == cols - 1)
		return 1;
	for (int i = 1; i < snake.getSize(); i++)
	{
		if (snake.getXpart(i) == snake.x && snake.getYpart(i) == snake.y)
			return 1;
	}
	return 0;
}

int main()
{
	setlocale(LC_ALL, "ru");

	char var;
	bool stop = 0;
	do
	{
		cout << "������ ����� ����?" << endl;
		cout << "Y - ��, N - ���" << endl;
		cout << "������� Y ��� N, � ����� ������� Enter" << endl;
		cin >> var;
		if (var == 'N' || var == 'n')
			stop = 1;
		if (var == 'Y' || var == 'y')
		{
			cout << "�������� ���������: 1 - �����, 2 - ������, 3 - ������." << endl;
			cout << "������� ����� 1, 2 ��� 3: " << endl;
			int gamemode;
			cin >> gamemode;

			int speed;
			switch (gamemode)
			{
			case(1):
			{speed = 200;
			break; }
			case(2):
			{speed = 130;
			break; }
			case(3):
			{speed = 80;
			break; }
			}

			int rows = 15;
			int cols = 15;
			string** arr = new string * [rows];
			for (int i = 0; i < rows; i++)
			{
				arr[i] = new string[cols];
			}

			Snake snake;

			Apple sApple(1, '�'); //�
			Apple bApple(5, '$');

			system("cls");

			SetPos(20, 2);
			cout << "������ ����������� ���������.\t��� ����: " << snake.getScore() << endl;

			fillArray(arr, rows, cols);

			setSnakeOnArray(arr, snake); //start position snake

			showArray(arr, rows, cols);

			srand(time(0));

			int counter = 1; //needed for create BIG apple

			do
			{
				char button = _getch();
				while (!_kbhit())
				{
					deleteTail_FromArray(arr, snake);

					snake.move(button);

					setSnakeOnArray(arr, snake);

					if (gameover(rows, cols, snake))
						break;

					//setting random coordinate for small apple
					if (sApple.x == 0 && sApple.y == 0)
					{
						bool z = 0;
						int x = (rand() % (rows - 3) + 1);
						int y = (rand() % (cols - 3) + 1);
						if ((x != bApple.x && y != bApple.y) || (x != snake.x && y != snake.y))
						{
							for (int i = 0; i < snake.getSize(); i++)
							{
								if (!(snake.getXpart(i) == x && snake.getYpart(i) == y))
									z++;
								else
								{
									z = 0;
									break;
								}
							}
						}
						if (z)
							sApple.setXY(x, y);
					}
					//setting random coordinate for BIG apple
					if (counter % 30 == 0 && bApple.x == 0 && bApple.y == 0)
					{
						bool z = 0;
						int x = (rand() % (rows - 3) + 1);
						int y = (rand() % (cols - 3) + 1);
						if ((x != sApple.x && y != sApple.y) || (x != snake.x && y != snake.y))
						{
							for (int i = 0; i < snake.getSize(); i++)
							{
								if (!(snake.getXpart(i) == x && snake.getYpart(i) == y))
									z++;
								else
								{
									z = 0;
									break;
								}
							}
						}
						if (z)
							bApple.setXY(x, y);
					}

					//the condition of eaten apple
					//and setting apple coordinate to zero
					if (snake.x == sApple.x &&
						snake.y == sApple.y)
					{
						snake.EatAnApple(sApple);
						sApple.setXY(0, 0);
					}
					if (snake.x == bApple.x &&
						snake.y == bApple.y)
					{
						snake.EatAnApple(bApple);
						bApple.setXY(0, 0);
						counter = 1;
					}

					setAppleOnArray(arr, rows, cols, sApple, bApple);

					SetPos(20, 2);
					cout << "������ ����������� ���������.\t��� ����: " << snake.getScore() << endl;

					counter++;
					Sleep(speed);
				}
				if (gameover(rows, cols, snake))
				{
					cout << char(7);
					stop = 0;
					break;
				}
			} while (true);

			for (int i = 0; i < rows; i++)
			{
				delete[] arr[i];
			}
			delete[] arr;

			SetPos(23, 0);
			cout << "�� ���������!" << endl;
			cout << "��� ����: " << snake.getScore() << endl << endl;
			Sleep(500);
		}
	} while (!stop);
}

//void main()
//{
//	//setlocale(LC_ALL, "ru");
//
//	for (int j = 1; j < 260; j++)
//	{
//		cout << j << "\t" <<(char) j << endl;
//		//cout << _kbhit() << endl;
//		//Sleep(500);
//	}
//} 